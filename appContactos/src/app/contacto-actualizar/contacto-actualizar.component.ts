import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { Contacto } from '../model/contacto.model';
import { ContactoApi } from '../model/contacto-API.model';
import { elegidoFavoritoAction, nuevoContactoAction } from '../model/contacto-status.model';

@Component({
  selector: 'app-contacto-actualizar',
  templateUrl: './contacto-actualizar.component.html',
  styleUrls: ['./contacto-actualizar.component.css']
})
export class ContactoActualizarComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Contacto>
  cList : Contacto[]

  constructor(private contactosApi: ContactoApi, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter()
    this.cList = this.contactosApi.getAll() 
  }

  ngOnInit(): void {
  }

  agregado(c: Contacto) {
    this.contactosApi.add(c)
    this.onItemAdded.emit(c)
    this.store.dispatch(new nuevoContactoAction(c))
  }

  actualizar(key: string,  c: Contacto){
    this.contactosApi.modificar(key, c);
  }

}
