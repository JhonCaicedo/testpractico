import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState} from '../app.module';
import { Contacto } from '../model/contacto.model';
import { ContactoApi } from '../model/contacto-API.model';
import { elegidoFavoritoAction, nuevoContactoAction } from '../model/contacto-status.model';

@Component({
  selector: 'app-contacto-lista',
  templateUrl: './contacto-lista.component.html',
  styleUrls: ['./contacto-lista.component.css']
})
export class ContactoListaComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Contacto>
  updates: string[]
  cList : Contacto[]

  constructor( private contactosApi:ContactoApi, private store: Store<AppState>) { 
    this.updates = []
    //this.store.select(state => state.contactos.favorito)
    //.subscribe(c => {
    //  if(c != null){
    //    this.updates.push('Se ha elegido a ' + c.nombre + ', como favorito.')
    //  }
    //})
    this.cList = this.contactosApi.getAll() 
    
  }

  ngOnInit(): void {
       
  }

  elegido(c: Contacto) {
    this.contactosApi.elegir(c)
    this.store.dispatch(new elegidoFavoritoAction(c))
  }
  

}
