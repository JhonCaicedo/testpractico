import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Contacto } from './contacto.model';
import { isNgTemplate } from '@angular/compiler';

export interface ContactoStatus {
    items: Contacto[]
    loading: boolean
    favorito: Contacto
}

export const intializeContactosStatus = function () {
    return {
        items: [],
        loading: false,
        favorito: null
    }
}

export enum contactoActionTypes {
    NUEVO_CONTACTO = '[Contacto] Nuevo',
    ELEGIDO_FAVORITO = '[Contacto] Favorito'
}

export class nuevoContactoAction implements Action {
    type = contactoActionTypes.NUEVO_CONTACTO
    constructor(public contacto: Contacto) {

    }
}

export class elegidoFavoritoAction implements Action {
    type = contactoActionTypes.ELEGIDO_FAVORITO
    constructor(public contacto: Contacto) { }
}

export type contactoActions = nuevoContactoAction | elegidoFavoritoAction

export function reducerContacto(
    state: ContactoStatus,
    action: contactoActions
): ContactoStatus {
    switch (action.type) {
        case contactoActionTypes.NUEVO_CONTACTO: {
            return {
                ...state,
                items: [...state.items, (action as nuevoContactoAction).contacto]
            }
        }
        case contactoActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false))
            this.contacto.set
            const fav: Contacto = (action as elegidoFavoritoAction).contacto
            fav.setSelected(true)
            return {
                ...state,
                favorito: fav
            }
        }
    }
}

@Injectable()
export class ContactoEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(contactoActionTypes.NUEVO_CONTACTO),
        map((action: nuevoContactoAction) => new elegidoFavoritoAction(action.contacto))
    )
    constructor(private actions$: Actions) { }
}
