export class Contacto {
    private selected: boolean;
    public id: number;

    constructor(
        public nombre: string, 
        public telefono: string, 
        public correo: string) {
        this.id = 0
        this.selected = false
    }

    isSelected(): boolean {
        return this.selected
    }

    setSelected(s: boolean) {
        this.selected = s
    }
}