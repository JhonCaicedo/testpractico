import { BehaviorSubject, Subject } from 'rxjs';
import { Contacto } from './contacto.model';

export class ContactoApi {
    contactos: Contacto[]
    current: Subject<Contacto>
    contador: number

    constructor() {
        this.contactos = []
        this.contador = localStorage.length
        localStorage.clear()
    }

    add(c: Contacto) {
        localStorage.setItem(this.contador.toString(), JSON.stringify(c));
        this.contador = this.contador + 1;
    }

    getAll(): Contacto[] {
        if(localStorage.length === this.contactos.length){
            return this.contactos
        }else if(localStorage.length > this.contactos.length && this.contactos.length > 0){
            for (let i = this.contador - 1 ; i < localStorage.length; i++) {
                let json = JSON.parse(localStorage.getItem(i.toString()))
                let contacto = new Contacto(json['nombre'], json['telefono'], json['correo'])
                this.contactos.push(contacto)
            }
            return this.contactos
        }else if(localStorage.length < this.contactos.length && this.contactos.length > 0){
            this.contactos = []
            for (let i = this.contador - 1 ; i < localStorage.length; i++) {
                let json = JSON.parse(localStorage.getItem(i.toString()))
                let contacto = new Contacto(json['nombre'], json['telefono'], json['correo'])
                this.contactos.push(contacto)
            }
            return this.contactos
        }else{
            for (let i = 0; i < localStorage.length; i++) {
                let json = JSON.parse(localStorage.getItem(i.toString()))
                let contacto = new Contacto(json['nombre'], json['telefono'], json['correo'])
                this.contactos.push(contacto)
            }
            return this.contactos
        }
        
    }

    modificar(key: string, c: Contacto) {
        localStorage.setItem(key, JSON.stringify(c));
    }

    getById(id: string): Contacto {
        return this.contactos.filter(function (c) {
            return c.id.toString() === id
        })[0]
    }

    elegir(c: Contacto) {
        this.contactos.forEach(x => x.setSelected(false))
        c.setSelected(true)
        this.current.next(c)
    }

    subscribenOnChange(fn) {
        this.current.subscribe(fn)
    }
}
