import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { Contacto } from '../model/contacto.model';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
  @Input() contacto: Contacto
  @Input() position: number
  @HostBinding('attr.class') cssClass = 'col-md-4' 
  @Output() clicked: EventEmitter<Contacto>

  constructor() { 
    this.clicked = new EventEmitter()
  }

  ngOnInit(): void {
  }

  marcar(){
    this.contacto.setSelected(true);
    this.clicked.emit(this.contacto);
    return false;
  }
}
