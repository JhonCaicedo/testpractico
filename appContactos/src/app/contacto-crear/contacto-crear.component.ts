import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { Contacto } from '../model/contacto.model';
import { ContactoApi } from '../model/contacto-API.model';
import { elegidoFavoritoAction, nuevoContactoAction } from '../model/contacto-status.model';


@Component({
  selector: 'app-contacto-crear',
  templateUrl: './contacto-crear.component.html',
  styleUrls: ['./contacto-crear.component.css']
})
export class ContactoCrearComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Contacto>

  constructor(private contactosApi: ContactoApi, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter()
  }

  ngOnInit(): void {
  }
  
  agregado(c: Contacto) {
    this.contactosApi.add(c)
    this.onItemAdded.emit(c)
    this.store.dispatch(new nuevoContactoAction(c))
  }
}
