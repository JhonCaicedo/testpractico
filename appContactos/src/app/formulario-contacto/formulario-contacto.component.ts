import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Contacto } from '../model/contacto.model';

@Component({
  selector: 'app-formulario-contacto',
  templateUrl: './formulario-contacto.component.html',
  styleUrls: ['./formulario-contacto.component.css']
})
export class FormularioContactoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Contacto>
  @Input() contacto: Contacto
  
  fg: FormGroup
  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter()
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required
      ])],
      telefono: ['', Validators.compose([
        Validators.required
      ])],
      correo: ['', Validators.compose([
        Validators.required
      ])]
    })

    this.fg.valueChanges.subscribe(
      (form: any) =>{
        console.log('Cambio en el formulario: ' + form)
      }
    )
  }

  ngOnInit(): void {
    
  }

  guardar(nombre:string, telefono:string, correo:string): boolean{
    const c = new Contacto(nombre, telefono, correo)
    this.onItemAdded.emit(c)
    return false
  }

}
