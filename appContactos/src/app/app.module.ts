import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule as NgRxStoreModule} from '@ngrx/store';
import { RouterModule, Routes} from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactoComponent } from './contacto/contacto.component';
import { ContactoListaComponent } from './contacto-lista/contacto-lista.component';
import { FormularioContactoComponent } from './formulario-contacto/formulario-contacto.component';
import { ContactoEffects, ContactoStatus, intializeContactosStatus, reducerContacto} from './model/contacto-status.model';
import { ActionReducerMap } from '@ngrx/store';
import { ContactoApi } from './model/contacto-API.model';
import { ContactoCrearComponent } from './contacto-crear/contacto-crear.component';
import { ContactoActualizarComponent } from './contacto-actualizar/contacto-actualizar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
  {path: '', redirectTo: 'lista', pathMatch: 'full'},
  {path: 'lista', component: ContactoListaComponent},
  {path: 'crea', component: ContactoCrearComponent},
  {path: 'modifica', component: ContactoActualizarComponent}
];

export interface AppState{
  contactos: ContactoStatus;
}

const reducers: ActionReducerMap<AppState> = {
  contactos: reducerContacto
}

let reducersInitialStates ={
  contactos: intializeContactosStatus()
}

@NgModule({
  declarations: [
    AppComponent,
    ContactoComponent,
    ContactoListaComponent,
    FormularioContactoComponent,
    ContactoCrearComponent,
    ContactoActualizarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forRoot([ContactoEffects]),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialStates}),
    RouterModule.forRoot(routes),
    NgbModule
  ],
  providers: [ContactoApi],
  bootstrap: [AppComponent]
})
export class AppModule { }
