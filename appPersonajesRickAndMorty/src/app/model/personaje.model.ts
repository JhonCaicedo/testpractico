export class Personaje {
        
    constructor(
        public image:string, 
        public name:string, 
        public status:string, 
        public species:string, 
        public gender:string,
        public origin:JSON) { 
        
    }
}